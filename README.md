Este repositorio contiene los datos y comandos principales que utilizaremos en el **Taller de Análisis Factorial y Cluster** para el 
Taller de Análisis de Datos de Converciencia 2019.

---

#Factorial

El archivo 'PCA.py' contiene comandos necesarios para importar los datos y realizar el análisis de componentes principales. 

---

#Cluster

El archivo 'cluster.py' contiene comandos necesarios para importar los datos y realizar el análisis de cluster de partición y análisis 
cluster jerárquico.

---

#Datos

Los datos se ecnuentran en la carpeta 'data'. En la subcarpeta 'fuente' se encuentran los datos obtenidos directamente de los sitios web del
Ministerio de Educación y del Instituto Nacional de Estadística.